//
//  ViewController.swift
//  DummyCrash
//
//  Created by ItsDp on 21/07/20.
//  Copyright © 2020 ItsDp. All rights reserved.
//

import UIKit
import WebKit
class ViewController: UIViewController {

    var webview = WKWebView()

    override func viewDidLoad() {
        super.viewDidLoad()

            self.webview.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            self.view.addSubview(self.webview)
            self.webview.scrollView.bounces = false
            self.webview.scrollView.isScrollEnabled = true
        
     
            self.webview.scrollView.showsHorizontalScrollIndicator = false
            var request = URLRequest(url: URL.init(string: "http://trainingdev.stratbeans.com/syngenta/index.php?r=report/trainingHistory/mobile&token=IvKnbt9QJ4g68bv")!)
       
            self.webview.load(request)
        
        // Do any additional setup after loading the view.
    }


}

