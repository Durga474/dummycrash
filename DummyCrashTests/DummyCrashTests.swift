//
//  DummyCrashTests.swift
//  DummyCrashTests
//
//  Created by ItsDp on 21/07/20.
//  Copyright © 2020 ItsDp. All rights reserved.
//

import XCTest
@testable import DummyCrash

/// A simple struct containing a list of users.
struct UsersViewModel {
    let users: [String]

    var hasUsers: Bool {
        return !users.isEmpty
    }
}

/// A test case to validate our logic inside the `UsersViewModel`.
final class UsersViewModelTests: XCTestCase {

    /// It should correctly reflect whether it has users.
    func testHasUsers() {
        let viewModel = UsersViewModel(users: ["Antoine", "Jaap", "Lady"])
        XCTAssertTrue(viewModel.hasUsers)
    }
}

class DummyCrashTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        let viewModel = UsersViewModel(users: ["Ed", "Edd", "Eddy"])
            XCTAssert(viewModel.users.count == 3)
            XCTAssertTrue(viewModel.users.count == 3)
            XCTAssertEqual(viewModel.users.count, 3)
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
